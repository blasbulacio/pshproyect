const express = require('express');
const app = express();
const cron = require('node-cron');
const fetch = require('node-fetch');
const json2csvParser = require('json2csv').Parser;


const select_top_players_query = `select players.nickname, stats.score, stats.creation_date, players.p_image 
                            from stats 
                            inner join players on stats.player_id = players.player_id 
                            order by score 
                            desc limit 10;`;
let lastTimeGenerated = "";

const mysql      = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    port: 33061,
    database: 'psh_game_db',
    user: 'root',
    password: 'secret'
});

connection.connect(function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return;
    }
    console.log('connected as id ' + connection.threadId);
  });

cron.schedule("*/5 * * * *", () => {
    lastTimeGenerated = Date.now();
    let i = Math.floor(Math.random() * 11);
    for(let j = 0; j <= i; j++){
        fetch('https://randomuser.me/api')
        .then(res =>{
            if(res.ok) return res.json();
          })
        .then((res) => {
            let player_obj = res.results[0];
            const player = {
                nickname: player_obj.login.username,
                p_image: player_obj.picture.medium,
            }
            console.log(player);
            connection.query(`insert into players (nickname, p_image) values ("${player.nickname}","${player.p_image}")`);
            connection.query(`select player_id from players where nickname="${player.nickname}"`, (error,rows,fields)=>{
                if (error) throw error;
                const stat = {
                    player_id: rows[0].player_id,
                    score: Math.floor(Math.random() * 100)+1
                }
                connection.query(`insert into stats (player_id, score) values ("${stat.player_id}",${stat.score})`);                    
            } );
        
        }).catch(error => {
            console.log(error);
        })
    }
    
});

app.get('/api/topten',(req,res)=>{
    connection.query(   select_top_players_query, 
                        (error,rows,fields)=>{    
                            if(error) throw error;
                            return res.json({
                                top_ten: rows,
                                last_time_generated: lastTimeGenerated
                            })
    });
})

app.get('/api/getCSV',(req,res)=>{
    connection.query(   select_top_players_query, 
                        (error,rows,fields)=>{
                            if(error) throw error;
                            const jsonData = JSON.parse(JSON.stringify(rows));
                            const json2csvP = new json2csvParser({header: true});
                            const csv = json2csvP.parse(jsonData);
                            res.send(csv);
                        });    
})
const port = process.env.PORT || 3001;
app.listen(port,()=>console.log(`Escuchando el puerto ${port}`));