import React, { Component } from 'react';
import axios from 'axios';


class Stats extends Component {
    constructor(){
        super();
        this.state = {
          stats: [],
          time: ""
        };
        this.componentDidMount = this.componentDidMount.bind(this);
      }
    
    componentDidMount = () => {
        this.getTopTen()
    }

    getTopTen = () =>{
        axios.get("/api/topten").then(response=>{
            this.setState({
                stats: response.data.top_ten,
                time: response.data.last_time_generated
            })
        })
        setTimeout(this.getTopTen, 10000);
        console.log(this.state.stats)
    }
    saveCsv = () => {
        axios.get("/api/getCSV").then(response=>{
            const blob = new Blob([response.data], {type:"text/plain"});
            const url = URL.createObjectURL(blob);
            const link = document.createElement('a');
            link.download = "topTenStats.csv";
            link.href = url;
            link.click();
        })      
    }

    statsTimeMessage(){
        if (this.state.time === "") return <p> Todavía no se generaron stats nuevas </p>
        return <p>Se generaron stats hace {Math.floor((Date.now()-this.state.time)/60000)} min </p>
    }

    render() { 
        return (
            <div>
                {this.statsTimeMessage()}
                <button onClick={this.saveCsv} className="btn btn-primary btn-sm m-2">Obtener CSV</button>    
                { this.state.stats.map(player => (
                <ul key = {player.nickname}  >
                {this.state.stats.indexOf(player)+1}: {player.nickname}, {player.score}, {player.creation_date} 
                <img src={player.p_image}/>
                </ul>
                )) }     
            </div>
        );
    }
}
 
export default Stats;